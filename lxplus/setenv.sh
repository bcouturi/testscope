#!/bin/bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Hackish environment using the LCG views
# We need a version of DD4hep more recent than what is released, hence we override that one

export myconfig=x86_64-centos7-gcc9-opt
source /cvmfs/sft.cern.ch/lcg/views/LCG_96b/${myconfig}/setup.sh
source /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/DD4hep/v01-11-44-g11e8bc6/${myconfig}/bin/thisdd4hep.sh

export CMAKE_PREFIX_PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/LCIO/02.12.01/${myconfig}:${CMAKE_PREFIX_PATH}
export CMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}:/usr

