/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Conditions/ConditionIdentifier.h"
#include "Conditions/ConditionsRepository.h"
#include "DD4hep/ConditionDerived.h"
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/ExtensionEntry.h"
#include "DD4hep/Printout.h"
#include "DDCond/ConditionsContent.h"
#include "DDCond/ConditionsTags.h"
#include "Detector/Scope/DeScopeConditionCalls.h"

#include <map>
#include <memory>
#include <string>

using namespace dd4hep;

static long create_conditions_recipes( Detector& description, xml_h e ) {

  using namespace LHCb::Detector;
  auto                               requests = std::make_shared<ConditionsRepository>();
  std::map<std::string, std::string> alignment_locations;
  std::map<std::string, std::string> condition_locations;
  std::string                        location;

  // First read from the XML element the file "locations"
  for ( xml_coll_t i( e, _U( alignments ) ); i; ++i ) {
    xml_comp_t c                                           = i;
    alignment_locations[c.attr<std::string>( _U( type ) )] = c.attr<std::string>( _U( ref ) );
  }

  // All information we need to create the new conditions we will receive during the callbacks.
  // It is not necessary to cache information such as detector elements etc.
  auto scope_iovUpdate = std::make_shared<DeScopeConditionCall>( requests );
  // --------------------------------------------------------------------------
  // 1) Setup the addresses for the raw conditions (call: addLocation())
  // 2) Setup the callbacks and their dependencies:
  // 2.1) Static detector elements depend typically on nothing.
  //      These parameters normally were read during
  //      the detector construction in the XML define section.
  //      The callback ensures the creation of the objects
  // 2.2) IOV dependent detector elements depend on
  //      - the raw conditions data (aka the time dependent conditions) (see 1)
  //      - and the static detector element
  // 2.3) Typically parents have references to the next level children
  //      - Add reference to the child detector elements
  // --------------------------------------------------------------------------
  auto deScope   = description.detector( "scope" );
  auto scopeAddr = requests->addLocation( deScope, Keys::deltaKey, alignment_locations["system"], "scope" );
  auto scopeIOV  = requests->addDependency( deScope, Keys::deKey, scope_iovUpdate );
  scopeIOV.second->dependencies.push_back( scopeAddr.first );

  for ( auto& side : deScope.children() ) {
    auto        deSide    = side.second;
    std::string side_name = deSide.name();
    /*auto sideAddr = */ requests->addLocation( deSide, Keys::deltaKey, alignment_locations["side"], side_name );
    for ( auto& sensor : deSide.children() ) {
      auto        deSensor    = sensor.second;
      std::string sensor_name = deSensor.name();
      std::string cond_name   = side_name + sensor_name;
      /*auto sideAddr = */ requests->addLocation( deSensor, Keys::deltaKey, alignment_locations["sensor"], cond_name );
    }
  }

  typedef std::shared_ptr<ConditionsRepository> Ext_t;
  deScope.addExtension( new dd4hep::detail::DeleteExtension<Ext_t, Ext_t>( new Ext_t( requests ) ) );
  return 1;
}
DECLARE_XML_DOC_READER( LHCb_Scope_cond, create_conditions_recipes )
