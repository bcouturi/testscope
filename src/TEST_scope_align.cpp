/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*

This plugin is for DEBUGGING only, it loads the DD4hep_ConditionsManagerInstaller
which is not used in LHCb. This is for demonstration only

XXX CLEANUP NEEDED

*/
// Framework include files
#include "DD4hep/Alignments.h"
#include "DD4hep/AlignmentsProcessor.h"
#include "DD4hep/Detector.h"
#include "DD4hep/DetectorProcessor.h"
#include "DD4hep/Factories.h"
#include "DD4hep/Printout.h"
#include "DDAlign/AlignmentsCalib.h"

#include "DDCond/ConditionsContent.h"
#include "DDCond/ConditionsManager.h"
#include "DDCond/ConditionsSlice.h"

using namespace std;
using namespace dd4hep;
using namespace dd4hep::align;
using align::AlignmentsCalib;

using ConditionsManager = dd4hep::cond::ConditionsManager;
using ConditionsContent = dd4hep::cond::ConditionsContent;
using ConditionsSlice   = dd4hep::cond::ConditionsSlice;

static void print_world_trafo( AlignmentsCalib& calib, const std::string& path ) {
  DetElement d( calib.detector( path ) );
  Alignment  a = calib.slice.get( d, align::Keys::alignmentKey );
  if ( a.isValid() ) {
    const double* tr = a.worldTransformation().GetTranslation();
    printout( INFO, "Example", "++ World transformation of: %-32s  Tr:(%8.2g,%8.2g,%8.2g [cm])", path.c_str(), tr[0],
              tr[1], tr[2] );
    a.worldTransformation().Print();
    return;
  }
  Condition c = calib.slice.get( d, align::Keys::deltaKey );
  printout( WARNING, "Example", "++ Detector element:%s No alignment conditions present. Delta:%s", path.c_str(),
            c.isValid() ? "Present" : "Not availible" );
}

static int test_align( Detector& description, int argc, char** argv ) {
  string input, setup;
  for ( int i = 0; i < argc && argv[i]; ++i ) {
    if ( 0 == ::strncmp( "-input", argv[i], 4 ) )
      input = argv[++i];
    else if ( 0 == ::strncmp( "-setup", argv[i], 5 ) )
      setup = argv[++i];
  }


  description.apply( "DD4hep_ConditionsManagerInstaller", 0, (char**)0 );
  dd4hep::cond::ConditionsManager manager = dd4hep::cond::ConditionsManager::from( description );
  manager["PoolType"]                     = "DD4hep_ConditionsLinearPool";
  manager["UserPoolType"]                 = "DD4hep_ConditionsMapUserPool";
  manager["UpdatePoolType"]               = "DD4hep_ConditionsLinearUpdatePool";
  manager.initialize();

  const void* setup_args[] = {setup.c_str(), 0}; // Better zero-terminate
  description.apply( "DD4hep_ConditionsXMLRepositoryParser", 1, (char**)setup_args );
  // Now the deltas are stored in the conditions manager in the proper IOV pools
  const IOVType* iov_typ = manager.iovType( "run" );
  if ( 0 == iov_typ ) { except( "ConditionsPrepare", "++ Unknown IOV type supplied." ); }
  IOV                           req_iov( iov_typ, 1500 ); // IOV goes from run 1000 ... 2000
  shared_ptr<ConditionsContent> content( new ConditionsContent() );
  shared_ptr<ConditionsSlice>   slice( new ConditionsSlice( manager, content ) );
  ConditionsManager::Result     cres = manager.prepare( req_iov, *slice );
  cond::fill_content( manager, *content, *iov_typ );

  // Collect all the delta conditions and make proper alignment conditions out of them
  map<DetElement, Delta> deltas;
  const auto             coll = deltaCollector( *slice, deltas );
  auto                   proc = detectorProcessor( coll );

  proc.process( description.world(), 0, true );
  printout( INFO, "Prepare", "Got a total of %ld deltas for processing alignments.", deltas.size() );


  // ++++++++++++++++++++++++ Compute the tranformation matrices
  AlignmentsCalculator         alignCalc;
  AlignmentsCalculator::Result ares = alignCalc.compute( deltas, *slice );
  printout( INFO, "Example", "Setup %ld/%ld conditions (S:%ld,L:%ld,C:%ld,M:%ld) (A:%ld,M:%ld) for IOV:%-12s",
            slice->conditions().size(), cres.total(), cres.selected, cres.loaded, cres.computed, cres.missing,
            ares.computed, ares.missing, iov_typ->str().c_str() );

  printout( INFO, "Example", "=========================================================" );
  printout( INFO, "Example", "====  Alignment transformation BEFORE manipulation  =====" );
  printout( INFO, "Example", "=========================================================" );
  slice->pool->print( "*" );

  AlignmentsCalib calib( description, *slice );
  try { // These are only valid if alignments got pre-loaded!
    print_world_trafo( calib, "/world/ScopeDetector" );
    print_world_trafo( calib, "/world/Telescope/module_1" );
    print_world_trafo( calib, "/world/Telescope/module_1/sensor" );

    print_world_trafo( calib, "/world/Telescope/module_3" );
    print_world_trafo( calib, "/world/Telescope/module_3/sensor" );

    print_world_trafo( calib, "/world/Telescope/module_5" );
    print_world_trafo( calib, "/world/Telescope/module_5/sensor" );
    print_world_trafo( calib, "/world/Telescope/module_8/sensor" );
  } catch ( const std::exception& e ) { printout( ERROR, "Example", "Exception: %s.", e.what() ); } catch ( ... ) {
    printout( ERROR, "Example", "UNKNOWN Exception...." );
  }

  /// Let's change something:
  //   Delta delta(Position(333.0,0,0));
  //   calib.set(calib.detector("/world/Telescope"),Delta(Position(55.0,0,0)));
  //   calib.set(calib.detector("/world/Telescope/module_1"),delta);
  //   calib.set("/world/Telescope/module_3",delta);
  //   /// Commit transaction and push deltas to the alignment conditions
  //   calib.commit();

  //   printout(INFO,"Example","=========================================================");
  //   printout(INFO,"Example","====  Alignment transformation AFTER manipulation   =====");
  //   printout(INFO,"Example","=========================================================");
  //   if ( dump ) slice->pool->print("*");

  //   print_world_trafo(calib,"/world/Telescope");
  //   print_world_trafo(calib,"/world/Telescope/module_1");
  //   print_world_trafo(calib,"/world/Telescope/module_1/sensor");
  //   print_world_trafo(calib,"/world/Telescope/module_2/sensor");

  //   print_world_trafo(calib,"/world/Telescope/module_3");
  //   print_world_trafo(calib,"/world/Telescope/module_3/sensor");
  //   print_world_trafo(calib,"/world/Telescope/module_4/sensor");

  //   print_world_trafo(calib,"/world/Telescope/module_5");
  //   print_world_trafo(calib,"/world/Telescope/module_5/sensor");
  //   print_world_trafo(calib,"/world/Telescope/module_6/sensor");
  //   print_world_trafo(calib,"/world/Telescope/module_7/sensor");
  //   print_world_trafo(calib,"/world/Telescope/module_8/sensor");

  return 1;
}

// first argument is the type from the xml file
DECLARE_APPLY( TEST_scope_align, test_align )
