#include <optional>
#include <typeinfo>

// Framework includes
#include "Conditions/ConditionsReader.h"
#include "DD4hep/IOV.h"

// The gitCondDB itself
#include "GitCondDB.h"

/// Namespace for the AIDA detector description toolkit
namespace LHCb::Detector {

  /// Class supporting the interface of the LHCb conditions database to dd4hep
  class ConditionsGitReader : public ConditionsReader {

    std::optional<GitCondDB::CondDB> m_condDB             = std::nullopt;
    std::string                      m_dbtag              = "";
    long                             m_validityLowerLimit = 0;
    long                             m_validityUpperLimit = dd4hep::IOV::MAX_KEY;

  public:
    /// Standard constructor
    ConditionsGitReader();
    /// Default destructor
    virtual ~ConditionsGitReader() {}
    /// Read raw XML object from the database / file
    virtual int getObject( const std::string& system_id, UserContext* ctxt, std::string& data ) override;
    /// Inform reader about a locally (e.g. by XercesC) handled source load
    virtual void parserLoaded( const std::string& system_id ) override;
    /// Inform reader about a locally (e.g. by XercesC) handled source load
    virtual void parserLoaded( const std::string& system_id, UserContext* ctxt ) override;
    /// Resolve a given URI to a string containing the data
    virtual bool load( const std::string& system_id, std::string& buffer ) override;
    /// Resolve a given URI to a string containing the data
    virtual bool load( const std::string& system_id, UserContext* ctxt, std::string& buffer ) override;
    /// Set the directory and rebuild the git cond db accordingly
    virtual void setDirectory( const std::string& dir ) override;
  };
} /* End namespace LHCb::Detector          */

//==========================================================================
// Framework includes
#include "DD4hep/Detector.h"
#include "DD4hep/Factories.h"
#include "DD4hep/Path.h"
#include "DD4hep/Printout.h"

// C/C++ include files
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

/// Standard constructor
LHCb::Detector::ConditionsGitReader::ConditionsGitReader() : ConditionsReader() {

  declareProperty( "ValidityLower", m_context.valid_since );
  declareProperty( "ValidityUpper", m_context.valid_until );
  declareProperty( "ValidityLowerLimit", m_validityLowerLimit );
  declareProperty( "ValidityUpperLimit", m_validityUpperLimit );
  declareProperty( "DBTag", m_dbtag );
}

void LHCb::Detector::ConditionsGitReader::setDirectory( const std::string& dir ) { m_directory = dir; }

int LHCb::Detector::ConditionsGitReader::getObject( const std::string& system_id, UserContext* ctxt,
                                                    std::string& buffer ) {

  if ( !m_condDB ) { m_condDB.emplace( GitCondDB::connect( m_directory ) ); }

  ConditionsReaderContext* ct = (ConditionsReaderContext*)ctxt;
  std::string path   = system_id;

  // Now loading the condition
  auto [data, iov] = m_condDB->get( {m_dbtag, path, static_cast<GitCondDB::CondDB::time_point_t>( ct->event_time )} );

  buffer           = data;
  ct->valid_since  = iov.since;
  ct->valid_until  = iov.until;
  return 1;
}

/// Resolve a given URI to a string containing the data
bool LHCb::Detector::ConditionsGitReader::load( const std::string& system_id, std::string& buffer ) {
  return dd4hep::xml::UriReader::load( system_id, buffer );
}

/// Resolve a given URI to a string containing the data
bool LHCb::Detector::ConditionsGitReader::load( const std::string& system_id, UserContext* ctxt, std::string& buffer ) {
    getObject(system_id, ctxt, buffer);
    return true;
}

/// Inform reader about a locally (e.g. by XercesC) handled source load
void LHCb::Detector::ConditionsGitReader::parserLoaded( const std::string& system_id ) {
  ConditionsReader::parserLoaded( system_id );
}

/// Inform reader about a locally (e.g. by XercesC) handled source load
void LHCb::Detector::ConditionsGitReader::parserLoaded( const std::string& /* system_id */, UserContext* ctxt ) {
  ConditionsReaderContext* ct = (ConditionsReaderContext*)ctxt;
  // Adjust IOV period according to setup (files have no IOV)
  ct->valid_since = m_context.valid_since;
  ct->valid_until = m_context.valid_until;
  // Check lower bound
  if ( ct->valid_since < m_validityLowerLimit )
    ct->valid_since = m_validityLowerLimit;
  else if ( ct->valid_since > m_validityUpperLimit )
    ct->valid_since = m_validityUpperLimit;
  // Check upper bound
  if ( ct->valid_until < m_validityLowerLimit )
    ct->valid_until = m_validityLowerLimit;
  else if ( ct->valid_until > m_validityUpperLimit )
    ct->valid_until = m_validityUpperLimit;
}

namespace {
  void* create_dddb_xml_file_reader( dd4hep::Detector&, int, char** ) {
    return new LHCb::Detector::ConditionsGitReader();
  }
} // namespace
DECLARE_DD4HEP_CONSTRUCTOR( LHCb_ConditionsGitReader, create_dddb_xml_file_reader )
