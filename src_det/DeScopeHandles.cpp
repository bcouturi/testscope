//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================

// A bit of magic you do not really want to know about....

// Framework include files
#include "DD4hep/detail/Handle.inl"
#include "Detector/Scope/DeScope.h"

using namespace LHCb::Detector::detail;

DD4HEP_INSTANTIATE_HANDLE_UNNAMED( DeScopeObject, DeIOVObject, ConditionObject );
DD4HEP_INSTANTIATE_HANDLE_UNNAMED( DeScopeSideObject, DeIOVObject, ConditionObject );
DD4HEP_INSTANTIATE_HANDLE_UNNAMED( DeScopeSensorObject, DeIOVObject, ConditionObject );
