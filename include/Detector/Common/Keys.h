/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Common/DeIOV.h"
#include "Detector/Common/ParameterMap.h"

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/ConditionsMap.h"
#include "DD4hep/DetElement.h"
#include "DD4hep/Objects.h"
#include "DD4hep/Printout.h"
#include "DD4hep/detail/ConditionsInterna.h"

#include "Math/Point3D.h"
#include "Math/Vector3D.h"

#include <map>

namespace LHCb::Detector {

  /// Static identifiers computed once. Use the hash code whenever possible!
  struct Keys {
    /// Static key name: "DetElement-Info-IOV"
    static const std::string deKeyName;
    /// Static key: 32 bit hash of "DetElement-Info-IOV". Must be unique for one DetElement
    static const dd4hep::Condition::itemkey_type deKey;

    /// Key name  of a delta condition "alignment_delta".
    static const std::string deltaName;
    /// Key value of a delta condition "alignment_delta".
    static const dd4hep::Condition::itemkey_type deltaKey;

    /// Static key name: "alignment"
    static const std::string alignmentKeyName;
    /// Static key: 32 bit hash of "alignment". Must be unique for one DetElement
    static const dd4hep::Condition::itemkey_type alignmentKey;

    /// Static key name: "Alignments-Computed"
    static const std::string alignmentsComputedKeyName;
    /// Static key: 32 bit hash of "Alignments-Computed". Must be unique for the world
    static const dd4hep::Condition::key_type alignmentsComputedKey;
  };
} // End namespace LHCb::Detector
