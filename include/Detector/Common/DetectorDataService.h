/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Conditions/ConditionsRepository.h"
#include "DDCond/ConditionsContent.h"
#include "DDCond/ConditionsDataLoader.h"
#include "DDCond/ConditionsManager.h"
#include "DDCond/ConditionsManagerObject.h"
#include "DDCond/ConditionsSlice.h"

#include <map>
#include <memory>
#include <vector>

namespace LHCb::Detector {

  class DetectorDataService {

  public:
    DetectorDataService( dd4hep::Detector& description, std::vector<std::string> detectorNames )
        : m_description( description )
        , m_manager( description, "DD4hep_ConditionsManager_Type1" )
        , m_detectorNames( detectorNames ) {}

    void initialize( std::string loadingDir, std::string dbTag );
    void finalize();

    std::shared_ptr<dd4hep::cond::ConditionsSlice> get_slice( size_t iov );
    std::shared_ptr<dd4hep::cond::ConditionsSlice> load_slice( size_t iov );

  protected:
    dd4hep::Detector&                     m_description;
    dd4hep::cond::ConditionsManager       m_manager;
    std::vector<std::string>              m_detectorNames;
    const dd4hep::IOVType*                m_iov_typ{nullptr};
    std::shared_ptr<ConditionsRepository> m_all_conditions = std::make_shared<ConditionsRepository>();
    std::map<size_t, std::shared_ptr<dd4hep::cond::ConditionsSlice>> m_cache;
    std::mutex                                                       m_cache_mutex;
  };

  const std::type_info& get_condition_type( const dd4hep::Condition& condition ) {
    if ( condition.is_bound() )
      return condition.typeInfo();
    else
      return typeid( *condition );
  }

} // End namespace LHCb::Detector
